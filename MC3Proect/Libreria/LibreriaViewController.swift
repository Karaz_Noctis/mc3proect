//
//  LibreriaViewController.swift
//  MC3Proect
//
//  Created by Francesco Spigno on 11/02/2020.
//  Copyright © 2020 KarazIndustries. All rights reserved.
//

import UIKit

class LibreriaViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var librerie : [Libreria] = [
    Libreria(image: UIImage(named: "myblood")!),
    Libreria(image: UIImage(named: "myblood")!)
    ]
    
    let cellIdentifier = "LibCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self

        
    }
    

  

}

extension LibreriaViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return librerie.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? LibreiraCollectionViewCell else {
            fatalError("Error")
        }
        
        cell.pgImage.image = librerie[indexPath.item].image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 200)
    }
    
}
