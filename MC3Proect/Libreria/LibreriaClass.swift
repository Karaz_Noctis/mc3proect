//
//  LibreriaClass.swift
//  MC3Proect
//
//  Created by Francesco Spigno on 11/02/2020.
//  Copyright © 2020 KarazIndustries. All rights reserved.
//

import Foundation
import UIKit

class Libreria{
    
    var image : UIImage
    
    init(image : UIImage) {
        self.image = image
    }
    
}
