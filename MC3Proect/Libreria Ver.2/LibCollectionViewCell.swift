//
//  LibCollectionViewCell.swift
//  MC3Proect
//
//  Created by Francesco Spigno on 20/02/2020.
//  Copyright © 2020 KarazIndustries. All rights reserved.
//

import UIKit

class LibCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var eddyPic: UIImageView!
    @IBOutlet weak var nameCard: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
