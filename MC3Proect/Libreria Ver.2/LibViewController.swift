//
//  LibViewController.swift
//  MC3Proect
//
//  Created by Francesco Spigno on 20/02/2020.
//  Copyright © 2020 KarazIndustries. All rights reserved.
//

struct Item {
    var image : UIImage!
    var label : String
}

struct Patate {
    var imagePatane : UIImage!
    var quantità : String
}

import UIKit

class LibViewController: UIViewController {
    
    let cellId = "cellId"
    
    var collectionViewFlowLayout : UICollectionViewFlowLayout!
    
    var items : [Item] = [
        Item(image: UIImage(named: "circles")!, label: "Fantastcio"),
        Item(image: UIImage(named: "myblood")!, label: "Superbo"),
        Item(image: UIImage(named: "radioactive")!, label: "Bellissimo"),
        Item(image: UIImage(named: "familiar")!, label: "Jesus")]
    
    var potatoes : [Patate] = [
        Patate(imagePatane: UIImage(named: ""), quantità: "4")
    ]

    @IBOutlet weak var segmentedCollection: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var page : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        page = 0

        // Do any additional setup after loading the view.
        
        let nib = UINib(nibName: "LibCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: cellId)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewWillLayoutSubviews() {
        setupCollectionView()
    }
    
    
    private func setupCollectionView(){
        if collectionViewFlowLayout == nil{
        let lineSpacing : CGFloat = 15
        let interItemSpacing : CGFloat = 10
            
        collectionViewFlowLayout = UICollectionViewFlowLayout()
        
        collectionViewFlowLayout.minimumLineSpacing = lineSpacing
        collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        
        
        collectionViewFlowLayout.itemSize = CGSize(width: 180, height: 250)
        
        collectionView.setCollectionViewLayout(collectionViewFlowLayout, animated : true)
        }
    }
    
    
    @IBAction func segmentedAction(_ sender: Any) {
        page = segmentedCollection.selectedSegmentIndex
        collectionView.reloadData()
    }
    
}


extension LibViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? LibCollectionViewCell else{
            fatalError("Nope")
        }
        
        cell.eddyPic.image = items[indexPath.row].image
        cell.nameCard.text = items[indexPath.row].label
        
        return cell
    }
}
