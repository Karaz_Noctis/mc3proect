//
//  Character.swift
//  MC3Proect
//
//  Created by Francesco Spigno on 11/02/2020.
//  Copyright © 2020 KarazIndustries. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class Personaggio{
    
    var titolo : String
    
    var categoria : String
    
    var elmo : UIImage
    var armatura : UIImage
    var stivali : UIImage
    var manoDx : UIImage
    var manoSx : UIImage
    
    init(elmo : UIImage, armatura : UIImage, stivali : UIImage, manoDx : UIImage, manoSx : UIImage, titolo : String, categoria : String) {
        
        self.titolo = titolo
        
        self.categoria = categoria
        
        self.elmo = elmo
        self.armatura = armatura
        self.stivali = stivali
        self.manoDx = manoDx
        self.manoSx = manoSx
        self.categoria = categoria
        
    }
}
