//
//  ViewController.swift
//  MC3Proect
//
//  Created by Francesco Spigno on 11/02/2020.
//  Copyright © 2020 KarazIndustries. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

    @IBAction func segueToSingle(_ sender: Any) {
        self.performSegue(withIdentifier: "toSingle", sender: nil)
    }
    
    @IBAction func segueToMulti(_ sender: Any) {
        self.performSegue(withIdentifier: "toMulti", sender: nil)
    }
    
    @IBAction func segueToCollection(_ sender: Any) {
        self.performSegue(withIdentifier: "toCollection", sender: nil)
    }
    
    @IBAction func segueToSettings(_ sender: Any) {
        self.performSegue(withIdentifier: "toSettings", sender: nil)
    }
    
}

